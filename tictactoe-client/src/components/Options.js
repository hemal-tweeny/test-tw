import React from 'react';
import Button from './Button'

const Options = ({logo, onChoice}) => {
    return (
        <>
        <div className='choice-container'>
            <a href="/"><img src={logo} alt='React TicTacToe'/></a>
            <Button onChoice={onChoice} type='primary' choice='new' label='Start New'/> 
            <Button onChoice={onChoice} type='secondary' choice='join' label='Join Game'/> 
        </div>
        </>
    );
}

export default Options;
