import './App.css';

import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';

import Home from './pages/Home'
// import Baord from './pages/Board'

function App() {
  return(
      <BrowserRouter>
        <main>
          <Routes>
              <Route path="/" element={<Home />} />
              {/* <Route path="game" element={<Baord />} /> */}
          </Routes>
        </main>
      </BrowserRouter>
  )
}

export default App;
